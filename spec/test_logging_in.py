"""
First steps file
"""
import requests
import pytest
from pytest_bdd import scenario, given, when, then, parsers
from simple_api import reset_db

@pytest.fixture
def pytestbdd_feature_base_dir():
    return './spec'

@pytest.fixture(scope='function')
def context():
    return {}

@scenario(
    'logging_in.feature', 
    'Creating new user',
    example_converters=dict(username=str, password=str, password_confirmation=str, phone=str, email=str))
def test_outlined():
    pass

@given("I want to create a new user account")
def create_user_account_url(context):
    context["url"] = "http://localhost:5000/addrec"

@when("I provide input <username>, <password>, <password_confirmation>, <email>, <phone>")
def provided_input(context, username, password, password_confirmation, email, phone):
    name_query = "name=" + username
    passw_query = "passw=" + password
    passw_conf_query = "passw_conf=" + password_confirmation
    email_query = "email=" + email
    if "+" in phone:
        phone = phone.replace("+","%2B")
    phone_query = "phone=" + phone
    context["phone"] = phone_query
    
    query = "?" + \
        name_query + "&" + \
        passw_query + "&" + \
        passw_conf_query + "&" + \
        email_query + "&" + \
        phone_query 
    
    result = requests.get("" + context["url"] + query)
    context["result"] = result.text

@then("I am notified about <resulting_message>")
def result_is(context, resulting_message):
    try:
        if str(context["result"]) == "New User Created":
            interpreted_result = "success"
        else:
            interpreted_result = "failure"
        assert interpreted_result == resulting_message, str(context["result"]) + " (phone: " + context["phone"] + ")"
    finally:
        requests.get("http://localhost:5000/reset")