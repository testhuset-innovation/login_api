Feature: API to use in performance test design and execution
    As a Performance Test Practitioner
    Do I need an API for use 
    In order to design & execute a realistic performance test plan against well-defined requirements to performance

    Scenario Outline: Creating new user
    Given I want to create a new user account
    When I provide input <username>, <password>, <password_confirmation>, <email>, <phone>
    Then I am notified about <resulting_message>

    Examples:
    | username  | password  | password_confirmation | email | phone | resulting_message |
    # Sunshine scenario
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 34453423 | success | 
    # Missing values
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | | 34453424 | failure |
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | |  | failure |
    # Password confirmation mismatch
    | user1     | dFsg-ln2.t_4 | dfsgln2t5  | user3@test.dk | 34453435 | failure |
    | user1     | dFsg-ln2.t_4 |  | user3@test.dk | 34453435 | failure |
    | user1     | dFsg-ln2.t_4 | DFsg-ln2.t_4  | user3@test.dk | 34453435 | failure |
    | user1     | dFsg-ln2.t_4 | dFsg- ln2.t_4  | user3@test.dk | 34453435 | failure |
    # Username length
    | 51characterusername51characterusername51characterss | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 34453423 | failure |
    | 51characterusername51 characterusername51characters | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 34453423 | failure |
    | 50characterusername51characterusername50characters | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 34453423 | success |
    | te | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 34453423 | failure |
    | tes | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 34453423 | success |
    # Password length
    | user1 | 51characterusername51characterusername51characterss | 51characterusername51characterusername51characterss  | user1@test.dk | 34453423 | failure |
    | user1 | 50characterusername51characterusername50characters | 50characterusername51characterusername50characters  | user1@test.dk | 34453423 | success |
    | user1 | 1234567 | 1234567  | user1@test.dk | 34453423 | failure |
    | user1 | 12345678 | 12345678  | user1@test.dk | 34453423 | success |
    # E-mail format
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1..@test.dk | 34453423 | failure |
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@..test.dk | 34453423 | failure |
    # Phone length and format
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | +4534453423344534231 | success |
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | +45344534233445342311 | failure |
    | user1     | dFsg-ln2.t_4 | dFsg-ln2.t_4  | user1@test.dk | 45+34453423344534231 | failure |
    
    Scenario Outline: Performance of Creating User
    Given <count of concurrent users> 
    And they attempt to create a new user account at the same time then accepted responce time is less than <responce in ms>
    
    Examples:
    | count of concurrent users | concurrent within ms | response in ms |
    | 10 | 1000 | 100 |
    | 100 | 1000 | 500 |
    | 1000 | 1000 | 1000 |
