FROM alpine:latest

RUN apk add --update py2-pip

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

COPY *.py /usr/src/app/
COPY src/* /usr/src/app/src/
COPY test/* /usr/src/app/test/

EXPOSE 5000

CMD ["python", "/usr/src/app/simple_api.py"]