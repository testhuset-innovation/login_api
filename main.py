from flask import Flask, redirect, url_for, request, render_template
from flask_login import LoginManager, current_user
from src.user import User

app = Flask(__name__)
login_manager = LoginManager()

login_manager.init_app(app) # initialize

@app.route('/login')
def login():
    return render_template('login.html')

@app.route('/')
def initial():
    if not current_user.is_authenticated:
        return redirect('login')
    else:
        return redirect('home')

@app.route('/authorize', methods = ['GET', 'POST'])
def authorize():
    if request.method == 'POST':
        user = request.form['usr']
        passw = request.form['passw']
    else:
        user = request.args.get('usr')
        passw = request.args.get('passw')
    return False
    

@login_manager.user_loader
def load_user(user_id):
    return None



if __name__ == '__main__':
    app.debug = True
    app.run() 