# README #

## Purpose ##

A simple login API.

## Setting it up ##

Prerequisites

- Docker

Getting Started

- pull from repo: git clone
- Make sure the final 3 lines in simple_api.py are:
    app.debug = True
    app.run(host="0.0.0.0")
    #app.run()
- run `docker build -t [TAG NAME] .` in PowerShell.
- run `docker run -p 5000:[PORT] [TAG NAME]`
- open browser and enter `localhost:[PORT]/list`. If it shows '[]', it works.

## Using it ##

Adding record
Open browser and type:
    localhost:[PORT]/addrec?name=[NAME]&passw=[PASSWORD]&passw_conf=[PASSWORD CONFIRMATION]&email=[EMAIL]&phone=[PHONE]
Where [PORT] was chosen in setup.

## Questions and maintainance ##

jri