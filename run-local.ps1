docker stop $(docker ps --filter ancestor=login-api --format "{{.Names}}")
docker build -t login-api .
docker run -p 5000:5000 login-api