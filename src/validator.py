import re

def validate(name, passw, passw_conf, email, phone):
    validated = True
    msg = ""
    if  not name \
        or not passw \
        or not passw_conf \
        or not email \
        or not phone:
        validated = False
        msg += "Missing Input\n"
    elif len(name) > 50:
        validated = False
        msg += "Username too long\n"
    elif len(name) < 3:
        validated = False
        msg += "Username too short\n"
    elif len(passw) > 50:
        validated = False
        msg += "Password too long\n"
    elif len(passw) == 0:
        validated = False
        msg += "No password entered\n"
    elif len(passw) < 8:
        validated = False
        msg += "Password too short\n"
    elif len(email) > 255:
        validated = False
        msg += "Email too long\n"
    elif len(phone) > 20:
        validated = False
        msg += "Phone number too long\n"
    elif passw != passw_conf:
        validated = False
        msg += "Password mismatch\n"
    elif not re.match(r'[\w]+', name):
        validated = False
        msg += "Not a valid username\n"
    elif not re.match(r'(\w+(\.|_|-)?)+@[\w-]+\.\w+$', email):
        validated = False
        msg += "Not a valid Email address\n"
    elif not re.match(r'(\+|00)?[1-9][0-9-]+$', phone):
        validated = False
        msg += "Not a valid phone number (" + phone + ")\n"
    return validated, msg