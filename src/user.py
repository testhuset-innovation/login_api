

class User():
    _user_id = None
    _authenticated = False
    _active = True
    _anonymous = False

    def __init__(self, user_id,):
        self._user_id = user_id

    def is_authenticated(self):
        auth = self._authenticated
        return auth

    def is_active(self):
        active = self._active
        return active

    def is_anonymous(self):
        anonymous = self._anonymous
        return anonymous

    def get_id(self):
        return self._user_id
