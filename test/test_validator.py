from src.validator import validate

class TestValidator():
    '''
    The validate function returns two values, result and msg.

    Result is a boolean that returns False is input violates some criteria.
    Msg is a string that returns the relevant error message, if any.
    '''
    
    def setup_method(self):
        '''
        These values are valid and assigned before each test.
        '''
        self.name = "user1"
        self.passw = "dfsgln2t4"
        self.passw_conf = "dfsgln2t4"
        self.email = "user1@test.dk"
        self.phone = "34453524"
    
    def test_sunshine(self):
        '''
        Uses initial valid data and should return True.
        '''
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert result

    def test_name_is_too_long(self):
        self.name = "kdiejifjsleikdiejifjsleikdiejifjsleikdiejifjsleikdiejifjslei"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_name_is_empty(self):
        self.name = ""
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_password_is_empty(self):
        self.passw = ""
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_password_not_confirmed(self):
        self.passw_conf = "diffpassword"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_phone_is_empty(self):
        self.phone = ""
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_phone_is_wrong_format(self):
        self.phone = "++23234345"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.phone = "0045a5434"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.phone = "+-23451342"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.phone = "34234+"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.phone = "243.234235"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
    
    def test_phone_is_too_long(self):
        self.phone = "234234242342342332121"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_email_is_empty(self):
        self.email = ""
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result

    def test_email_format_wrong(self):
        self.email = "user1@testdk"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.email = "user1(at)test.dk"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.email = "@test.dk"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.email = "user1@test."
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.email = "user1@test@dk"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
        # Next
        self.email = "user1@test/dk"
        result, msg = validate(self.name,self.passw,self.passw_conf,self.email,self.phone)
        assert not result
