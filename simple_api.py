import os
import re
import sqlite3 as sql
from flask import Flask, redirect, url_for, request, render_template, jsonify
import json
from src.validator import validate

app = Flask(__name__)
db = "simple.db"

@app.route('/list')
def list():
    if not db in os.listdir('.'):
        create_db()
    con = sql.connect(db)
    # con.row_factory = sql.Row

    cur = con.cursor()
    cur.execute("SELECT * FROM users")

    rows = cur.fetchall()
    return str(rows)

def create_db():
    conn = sql.connect(db)
    conn.execute('CREATE TABLE users (name CHARACTER(50) UNIQUE, passw CHARACTER(50), email CHARACTER(255), phone CHARACTER(20))')
    conn.close()

@app.route('/reset')
def reset_db():
    conn = sql.connect(db)
    conn.execute('DELETE FROM users')
    conn.commit()
    conn.close()
    return "reset"

@app.route('/auth', methods = ['POST', 'GET'])
def authorize():
    if not db in os.listdir('.'):
        create_db()
    con = sql.connect(db)
    if request.method == 'POST':
        name = request.form['name']
        passw = request.form['passw']
    else:
        name = request.args.get('name')
        passw = request.args.get('passw')
    try:
        result = ""
        with sql.connect(db) as con:
            cur = con.cursor()

            cur.execute("SELECT * FROM users WHERE name = ? and passw = ?", (name,passw) )
            result = cur.fetchone()
        if not result or len(result) == 0:
            msg = "Wrong Username or Password"
        else:
            msg = "You have looged in," + str(result)
    except:
        msg = "Error. Result: " + str(result)
    finally:
        con.close()
    return msg


@app.route('/addrec',methods = ['POST', 'GET'])
def addrec():
    if not db in os.listdir('.'):
        create_db()
    if request.method == 'POST':
        name = request.form['name']
        passw = request.form['passw']
        passw_conf = request.form['passw_conf']
        email = request.form['email']
        phone = request.form['phone']
    else:
        name = request.args.get('name')
        passw = request.args.get('passw')
        passw_conf = request.args.get('passw_conf')
        email = request.args.get('email')
        phone = request.args.get('phone')

    validated, validate_message = validate(name, passw, passw_conf, email, phone)
    if validated:
        try:
            with sql.connect(db) as con:
                cur = con.cursor()

                cur.execute("INSERT INTO users (name,passw,email,phone) \
                    VALUES (?,?,?,?)",(name,passw,email,phone) )

                con.commit()
            msg = "New User Created"
        except:
            con.rollback()
            msg = "User already exists"

        finally:
            con.close()
    else:
        msg = validate_message
    return msg

if __name__ == '__main__':
    app.debug = True # True while developing, False for production
    app.run(host="0.0.0.0") 
    #app.run()